import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
#include('MadGraphControl/setupEFT.py')
import random

#############################################################################################
### For simple processes only this part and the metadata at the bottom need to be changed


masses={'25': '1.250000e+02'}        ## Higgs mass
decays={'25': 'DECAY  25 4.07e-03'}  ## Higgs width


# create dictionary of processes this JO can create
definitions="""import model sm 
define p = g u c d s u~ c~ d~ s~
define j = p
define l+ = e+ mu+ 
define l- = e- mu-
"""
processes={#'mm':'generate p p > mu+ mu- mu+ mu- j j NP8==1\n', # name must not contain special characters, including _
           #'ee':'generate p p > e+ e- e+ e- j j NP8==1\n',
           'zzjj':'generate p p > z z j j QCD=0 QED=6\n'
}

nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob

# define cuts and other run card settings
settings={'dynamical_scale_choice':'2', # make sure to explicitly set a scale, otherwise it might end up being different for interference terms
        # see here for the scales: https://answers.launchpad.net/mg5amcnlo/+faq/2014
        'nevents': int(nevents),
        'asrwgtflavor':"5",
        'lhe_version':"3.0",
        'misset':"0",
        'ptj':"15",
        'ptb':"15",
        'pta':"0",
        'etaj':"5",
        'etab':"5",
        'drjj':"0",
        'draa':"0",
        'draj':"0",
        'mmjj':"10",
        'mmbb':"10",
        'mmll':"40",
        'drll':"0",
        'ptl':"4",
        'etal':"3.0",
        'drjl':"0",
        'dral':"0",
        #'mmnl':"130",
        'maxjetflavor':"5" ,
        'cut_decays'  :'T',
        'auto_ptj_mjj': 'F',
}
#############################################################################################

for p in processes:
    processes[p]=definitions+processes[p]+'output -f'

process_dir = new_process(processes['zzjj'])

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)
params={}
params['MASS']=masses
params['DECAY']=decays
modify_param_card(process_dir=process_dir,params=params)


rand_seed   = random.randint(1000,9999)
decaysyntax = "decay z > l+ l-"

madspin_card='madspin_card.dat'
mscard = open(madspin_card,'w')
mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
#Some options (uncomment to apply)
#set Nevents_for_max_weigth 75 # number of events for the estimate of the max. weight
#set BW_cut 15                # cut on how far the particle can be off-shell
#set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
set seed %i

#specify the decay for the final state particles
define l+ = e+ mu+    
define l- = e- mu-    
define vl = ve vm vt      
define vl~ = ve~ vm~ vt~  
define j = g u c d s u~ c~ d~ s~ 
set spinmode none
%s

# running the actual code
launch"""%(rand_seed,decaysyntax))
mscard.close()

generate(runArgs=runArgs,process_dir=process_dir)

add_madspin(madspin_card=madspin_card, process_dir=process_dir)

arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)  

# shower
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

# turn off mpi for faster generation and smaller files in 'noMPI' is part of name
if 'noMPI' in get_physics_short():
    genSeq.Pythia8.Commands += [' PartonLevel:MPI = off']

genSeq.Pythia8.Commands += [
            "SpaceShower:dipoleRecoil = on"]


#############################################################################################
# add meta data
evgenConfig.description = 'zzjj production with aTGC'
evgenConfig.keywords+=['zzjj']
evgenConfig.contact = ['Zuchen']
#############################################################################################
