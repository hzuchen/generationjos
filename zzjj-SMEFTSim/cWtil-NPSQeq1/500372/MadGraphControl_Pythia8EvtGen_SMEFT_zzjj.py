masses={'25': '1.250000e+02'}        ## Higgs mass
decays={'25': 'DECAY  25 4.07e-03'}  ## Higgs width

# definitions that will be used for process
definitions="""define p = g u c d s u~ c~ d~ s~
define j = p
define l+ = e+ mu+ 
define l- = e- mu- 
define vl = ve vm
define vl~ = ve~ vm~
"""

# create dictionary of processes this JO can create
processes={#'lljjEW' : 'generate p p > l+ l- j j  SMHLOOP==0 QCD==0 EFTORDER\n', # name must not contain special characters, including _
           'zzjj' : 'generate p p > z z j j SMHLOOP==0 QCD=0 EFTORDER\n',
           #'zzjj' : 'generate p p > e+ e- e+ e- j j SMHLOOP==0 QCD=0  EFTORDER\n',
           #'lljjQCD' : 'generate p p > l+ l- j j  SMHLOOP==0 QCD==2 EFTORDER\n',
           #'lljjQCDEW' : 'generate p p > l+ l- j j  SMHLOOP==0 QCD^2==2 EFTORDER\n',
           #'lljjQCDEW' : 'generate p p > l+ l- j j  SMHLOOP==0 EFTORDER\n',
}

# define cuts and other run card settings -- make sure to set a dynamic scale
settings={'dynamical_scale_choice':'2',
         #'nevents': int(nevents),
         'asrwgtflavor':"5",
         'lhe_version':"3.0",
         'misset':"0",
         'ptj':"15",
         'ptb':"15",
         'pta':"0",
         'etaj':"5",
         'etab':"5",
         'drjj':"0",
         'draa':"0",
         'draj':"0",
         'mmjj':"10",
         'mmbb':"10",
         'drll':"0",
         'ptl':"4",
         'etal':"3.0",
         'drjl':"0",
         'dral':"0",
         #'mmnl':"130",
         'maxjetflavor':"5" ,
         'cut_decays'  :'T',
         'auto_ptj_mjj': 'F',
         #
}

# param card settings (except EFT parameters)
params={}
params['MASS']=masses
params['DECAY']=decays

# only needed for reweighting:
relevant_coeffs = 'cG cW cHDD cHWB cHl1 cHl3 cHe cll1 clj1 clj3 cHj1 cHj3 cHu cHd ceu ced cje clu cld cjj11 cjj18 cjj31 cjj38 cuu1 cuu8 cdd1 cdd8 cud1 cud8 cju1 cju8 cjd1 cjd8'.split()

evgenConfig.description = 'zzjj production with SMEFTSIM'
evgenConfig.contact = ['Zuchen Huang <zuchen.huang@cern.ch>']

include("runSMEFT.py")
