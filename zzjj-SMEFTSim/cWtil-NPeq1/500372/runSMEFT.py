from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
import re,shutil

physics_short=get_physics_short()
#mgmodels='/cvmfs/atlas.cern.ch/repo/sw/Generators/madgraph/models/latest/'
mgmodels='/afs/cern.ch/work/h/hzuchen/Generator_Models/SMEFTsim-3.0.2/UFO_models/'

assert 'dynamical_scale_choice' in settings

# determine process from jo name
process=None
for p in processes:
    if p in physics_short.split('_'):
        process=definitions+processes[p]
        break
assert not process is None

# determine EFT parameter setting from jo name
params_re=r"_(?P<param>c[A-Za-z0-9]+)_(?P<value>[-+]?\d+p?\d*)"
eft_params_list=re.findall(params_re,physics_short)
eft_params={}
for e in eft_params_list:
    eft_params[e[0]]=e[1].replace('p','.')


# determine EFT order jo name
prop_corr=False
no_syst=False
assert 'EFTORDER' in process
actual_process=None
for e in physics_short.split('_'):
    if e.lower()=='sm':
        eft_order='NP==0'
        actual_process=process.replace('EFTORDER',eft_order)
        break
    elif e.lower()=='lin':
        eft_order='NP<=1 NP^2==1'
        if len(eft_params.keys())==1:
            eft_order=eft_order+" NP{}^2==1".format(eft_params.keys()[0])
        actual_process=process.replace('EFTORDER',eft_order)
        no_syst=True
        break
    elif e.lower()=='quad':
        eft_order='NP==1'
        if len(eft_params.keys())==1:
            eft_order=eft_order+" NP{}==1".format(eft_params.keys()[0])
        actual_process=process.replace('EFTORDER',eft_order)
        no_syst=True
        break
    elif e.lower()=='cross' or e.lower()=='x':
        assert len(eft_params.keys())==2
        eft_order='NP<=1 NP^2==2'
        eft_order=eft_order+" NP{}^2==1 NP{}^2==1".format(*(eft_params.keys()))
        actual_process=process.replace('EFTORDER',eft_order)
        no_syst=True
        break
    elif e.lower()=='full':
        eft_order='NP<=1'
        actual_process=process.replace('EFTORDER',eft_order)
        break
    elif e.lower()=='rwgtlin':
        eft_order='NP^2<=1'
        actual_process=process.replace('EFTORDER',eft_order)
        break
    elif e.lower()=='rwgtquad' or e.lower()=='rwgtcross' or e.lower()=='rwgtx':
        eft_order='NP<=1'
        actual_process=process.replace('EFTORDER',eft_order)
        break
    elif e.lower()=='linprop':
        eft_order='NP==0 NProp<=2 NPprop^2==2'
        actual_process=process.replace('EFTORDER',eft_order)
        prop_corr=True
        no_syst=True
        break
    elif e.lower()=='quadprop':
        eft_order='NP==0 NPprop==2'
        actual_process=process.replace('EFTORDER',eft_order)
        prop_corr=True
        no_syst=True
        break
assert actual_process is not None

if prop_corr:
    with open("pdgid_extras.txt","w") as f:
        for i in range(9000005,9000009):
            f.write(str(i)+'\n')
    testSeq.TestHepMC.UnknownPDGIDFile = 'pdgid_extras.txt'

# determine model from jo name
model=None
for m in physics_short.split('_'):
    if m=="SMEFTWp" or m=="SMEFTW" and prop_corr:
        model="SMEFTsim_topU3l_MwScheme_PropCorr_UFO"
        eft_parameter_block='SMEFT'
        restriction='massless'
        sm_card='param_card_massless.dat'
        break
    elif m=="SMEFTW":
        model="SMEFTsim_topU3l_MwScheme_UFO"
        eft_parameter_block='SMEFT'
        restriction='massless'
        sm_card='param_card_massless.dat'
        break
    elif m=="SMEFTap" or m=="SMEFTa" and prop_corr:
        model="SMEFTsim_topU3l_alphaScheme_PropCorr_UFO"
        eft_parameter_block='SMEFT'
        restriction='massless'
        sm_card='param_card_massless.dat'
        break
    elif m=="SMEFTa":
        model="SMEFTsim_topU3l_alphaScheme_UFO"
        eft_parameter_block='SMEFT'
        restriction='massless'
        sm_card='param_card_massless.dat'
        break
    elif m=="SMEFTWU35":
        model="SMEFTsim_U35_MwScheme_UFO"
        eft_parameter_block='SMEFT'
        restriction='massless'
        sm_card='param_card_CPV_massless.dat'
        break
    elif m=="SMEFTWU35V":
        model="SMEFTsim_U35_MwScheme_UFO"
        eft_parameter_block='SMEFTcpv'
        restriction='massless'
        sm_card='param_card_CPV_massless.dat'
        break
    elif m=="SMEFTWU35SM":
        model="SMEFTsim_U35_MwScheme_UFO"
        eft_parameter_block='SMEFTcpv'
        restriction='SMlimit_massless'
        sm_card='param_card_CPV_massless.dat'
        break
assert not model is None

actual_process='import model {}{}-{}\n{}\noutput -f'.format(mgmodels,model,restriction,actual_process)

import MadGraphControl.MadGraphUtils
MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING={
    'central_pdf':260000, 
    'pdf_variations': None if no_syst else [260000,90400],
    'alternative_pdfs':None if no_syst else [266000,265000,13100,25200], 
    'scale_variations':None if no_syst else [0.5,1.,2.], 
}
from MadGraphControl.MadGraphUtils import *

process_dir = new_process(actual_process)

# fetch default param card (all EFT parameters zero)
shutil.copy('/'.join([mgmodels,model,sm_card]),process_dir+'/Cards/param_card.dat')
# set eft parameters
modify_param_card(process_dir=process_dir,params={eft_parameter_block:eft_params})
# user set parameters
modify_param_card(process_dir=process_dir,params=params)

# modify run card
nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob
if not 'nevents' in settings:
    settings['nevents']=int(nevents)
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)


def reweight_smeft(p,order,coeffs,mode):
    reweight_text='change helicity False\n'
    ps=p.replace('\n',';').split(';')
    rwgt_ps=[]
    for p in ps:
        if 'generate' in p:
            rwgt_ps.append(p.replace('generate','change process').replace('EFTORDER',order))
        if 'add process' in p:
            rwgt_ps.append(p.replace('add process','change process').replace('EFTORDER',order)+' --add')
    reweight_text+='\n'.join(rwgt_ps)+'\n'    
    if mode==1:
        for c in coeffs:
            reweight_text+="launch --rwgt_name={0} --rwgt_info={0}\nset {0} 1\n".format(c)
            for cnot in coeffs:
                if not cnot==c:
                    reweight_text+="set {0} 0\n".format(cnot)
    if mode==2:
        from itertools import combinations
        for c1,c2 in combinations(coeffs,2):
            reweight_text+="launch --rwgt_name={0}_{1} --rwgt_info={0}_{1}\nset {0} 1\nset {1} 1\n".format(c1,c2)
            for cnot in coeffs:
                if not cnot in [c1,c2]:
                    reweight_text+="set {0} 0\n".format(cnot)
    return reweight_text

reweight_text=None
for r in physics_short.split('_'):    
    if r=='rwgtlin':
        reweight_text=reweight_smeft(process,'NP<=1 NP^2==1',relevant_coeffs,1)
        break
    if r=='rwgtquad':
        reweight_text=reweight_smeft(process,'NP==1',relevant_coeffs,1)
        break
    if r=='rwgtcross' or r=='rwgtx':
        reweight_text=reweight_smeft(process,'NP<=1 NP^2==2 '+' '.join(['NP{}^2<=1'.format(c) for c in relevant_coeffs]),relevant_coeffs,2)
        break

if not reweight_text is None:
    with open(process_dir+'/Cards/reweight_card.dat','w') as f:
        f.write(reweight_text)
        f.close()

generate(runArgs=runArgs,process_dir=process_dir)

arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)
   

# shower
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

# turn off mpi for faster generation and smaller files in 'noMPI' is part of name
if 'noMPI' in physics_short:
    genSeq.Pythia8.Commands += [' PartonLevel:MPI = off']

genSeq.Pythia8.Commands += [
            "SpaceShower:dipoleRecoil = on"]

genSeq.Pythia8.Commands += ["23:onMode = off",
                            "23:onIfAny = 11 13"]

