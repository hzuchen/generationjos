#!/bin/sh

#setupATLAS
source ~/.login
asetup 21.6.39.1,AthGeneration


Gen_tf.py --ecmEnergy=13000. --maxEvents=2000 --firstEvent=1 --randomSeed=999124 --outputEVNTFile=EVNT.root --jobConfig=/afs/cern.ch/work/h/hzuchen/GenerationJOs/zzjj-NTGC/f4a-Int/999124 

# the next step is only executed only if running on the batch to copy the relevent files
#cp EVNT.root log.generate /afs/cern.ch/work/h/hzuchen/dim8-MPI/output_f4a_NPSQeq1

