# This file was automatically created by FeynRules 2.3.12
# Mathematica version: 10.1.0  for Linux x86 (64-bit) (March 24, 2015)
# Date: Wed 6 Mar 2019 14:54:01


from object_library import all_orders, CouplingOrder


NP8 = CouplingOrder(name = 'NP8',
                    expansion_order = 1,
                    hierarchy = 1)

QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 1)

QED = CouplingOrder(name = 'QED',
                    expansion_order = 99,
                    hierarchy = 2)

